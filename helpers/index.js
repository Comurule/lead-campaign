const CreateOrUpdateCurrentBusiness = require('../helpers/CreateOrUpdateCurrentBusiness');
const CreateOrUpdatePermissions = require('../helpers/CreateOrUpdatePermissions');

module.exports = {
    CreateOrUpdateCurrentBusiness: CreateOrUpdateCurrentBusiness,
    CreateOrUpdatePermissions: CreateOrUpdatePermissions
};