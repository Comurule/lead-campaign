'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    profile_name: {
        type: DataTypes.STRING,
        allowNull: false,
        }
  });

  // create association between user and profile
  // a profile can have many users
  Profile.associate = (models) => {
    models.Profile.hasMany(models.User);
  };
  
  return Profile;
};
 