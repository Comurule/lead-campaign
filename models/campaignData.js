'use strict';
module.exports = (sequelize, DataTypes) => {
  const CampaignData = sequelize.define('CampaignData', {
    userResponse: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  });

  // create association between user and role
  // a can have many users
  CampaignData.associate = (models) => {
    models.CampaignData.belongsTo(models.User);
    models.CampaignData.belongsTo(models.Campaign);
    models.CampaignData.hasOne(models.LeadCampaignData);
  };

  return CampaignData;
};

