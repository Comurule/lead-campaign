module.exports = (sequelize, Sequelize) => {

    const dbVersion = sequelize.define('dbVersion', {
        version: Sequelize.STRING
    });

    return dbVersion;

}
