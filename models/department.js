'use strict';
module.exports = (sequelize, DataTypes) => {
  const Department = sequelize.define('Department', {
    departmentName: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });

  // create association between user and department
  // a department can have many users
  Department.associate = (models) => {
    models.Department.hasMany(models.User);
    models.Department.hasMany(models.Post);
  };

  return Department;
};
