'use strict';
module.exports = (sequelize, DataTypes) => {
  const Campaign = sequelize.define('Campaign', {
    campaignName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    campaignOwner: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  // create association between user and role
  // a can have many users
  Campaign.associate = (models) => {
    models.Campaign.belongsToMany(models.User, {
      as: 'users',
      through: 'CampaignMembers',
      foreignKey: 'campaignId'
    });

    models.Campaign.hasMany(models.CampaignData);
    models.Campaign.hasMany(models.LeadCampaignData);
    models.Campaign.hasMany(models.LeadCampaign);
  };

  return Campaign;
};

