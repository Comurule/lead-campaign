'use strict';
module.exports = (sequelize, DataTypes) => {
  const LeadCampaignData = sequelize.define('LeadCampaignData', {
    leadResponse: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  // create association between user and role
  // a can have many users
  LeadCampaignData.associate = (models) => {
    models.LeadCampaignData.belongsTo(models.User, {
      foreignKey: {
        name: 'leadId',
        allowNull: false
      }
    });
    models.LeadCampaignData.belongsTo(models.Campaign);
    models.LeadCampaignData.belongsTo(models.CampaignData);
  };

  return LeadCampaignData;
};

