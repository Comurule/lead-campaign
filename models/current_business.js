'use strict';
module.exports = (sequelize, DataTypes) => {
  const CurrentBusiness = sequelize.define('CurrentBusiness', {
    current_business_name: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });

  // create association between current business and user
  // a current business can have many users
  CurrentBusiness.associate = (models) => {
    models.CurrentBusiness.hasMany(models.User);
    models.CurrentBusiness.hasMany(models.Post);
  };

  return CurrentBusiness;
};

