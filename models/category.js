'use strict';
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    categoryName: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });

  // create association between category and post
  // an category can have many posts
  Category.associate = (models) => {
    models.Category.belongsToMany(models.Post, {
      as: 'posts',
      through: 'PostCategories',
      foreignKey: 'category_id'
    });
  };

  return Category;
};




// Make sure you complete other models fields